
var app = app || {}, myroot;

app.Settings = {
    //services_url    : "http://furazh.by/services/rest/",
    services_url    : "http://localhost:8080/services/rest/",

    addMessageUrl   : function() {
        return this.services_url+ "messages/add";
    },

    getContactListUrl   : function(sender_id){
        return this.services_url + "messages/getContactList?sender_id=" + sender_id;
    },

    getFilteredContactsUrl : function(filter_str, sender_id){
        return this.services_url+ "messages/getFilteredContacts?filter_str=" + filter_str + "&sender_id=" + sender_id;
    },

    getMessagesUrl : function(sender_id, recipient_id){
        return app.Settings.services_url+ "messages/my?sender_id=" + sender_id + "&recipient_id="+recipient_id;
    },

    getNewMessagesArray : function(sender_id){
        return this.services_url + "messages/getNewMessagesArray?sender_id=" + sender_id;
    },
    updateNewMessagesCount : function(sender_id, recipient_id){
        return this.services_url + "messages/updateNewMessageCount?sender_id=" + sender_id + "&recipient_id=" + recipient_id;
    }
};

app.me = {
  id : 1
};

app.selected_contact ={
    id : 2
};


function log(msg){
    console.log(msg);
}
