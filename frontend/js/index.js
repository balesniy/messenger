/* global Backbone, _, app */


app.Controller = Backbone.Router.extend({
    routes          : {
        ""              : "start"
    },
    start: function() {
        if(!this.mainView)
            this.mainView = new app.MainView().render();
    }
});


$(document).ready(function() {     
     myroot = new app.Controller();
     Backbone.history.start();
});
