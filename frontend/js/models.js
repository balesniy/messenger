/**
 * Created by jcnby on 25.01.16.
 */

app.ContactModel = Backbone.Model.extend({
    defaults : {
        id         : 0,
        recipient_id   : 0,
        messagesCount  : 0,
        nameRecipient  : "",
        selected: false
    }
});

//для обновления счетчиков новых сообщений
app.NewMessages = Backbone.Model.extend({
    defaults : {
        id         : 0,
        recipient_id   : 0,
        messageCount  : 0
    }
});

app.CollectionNewMessages = Backbone.Collection.extend({
    model: app.NewMessages,
    url : function(){ return app.Settings.getNewMessagesArray(app.me.id); }
});



app.MessageModel = Backbone.Model.extend({
    url : function(){ return app.Settings.addMessageUrl(); },
    defaults : {
        sender_id       : app.me.id,
        recipient_id    : 0,
        message         : '',
        time            : 0
    }
});


// общая коллекция, которая возвращает всех recipient с которыми есть message
app.CollectionContacts = Backbone.Collection.extend({
    model: app.ContactModel,
    url: function(){ return app.Settings.getContactListUrl(app.me.id); }
});

app.CollectionMessages = Backbone.Collection.extend({
    model: app.MessageModel,
    comparator: 'id',
    url : function(){ return app.Settings.getMessagesUrl(app.me.id, app.selected_contact.id); }
});
