app.contactPanelTemplate = '<div class="col-md-4" id="contactContent" style="background-color:#c6e2ff;min-width:320px;max-width:320px;min-height:550px;max-height:550px">\
</div>';

app.chatPanelTemplate = '<div class="col-md-8" id="chatContent" style="background-color:#e0ffff; min-width:480px;max-width:480px;min-height:550px;max-height:550px">\
</div>';

app.contactSearchTemplate = '<div class="row" align="left" style="margin-bottom: 20px;min-width:320px;max-width:320px;min-height:50px;max-height:50px">\
<div class="col-md-8">\
<input type="text" class="form-control" placeholder="Введите текст">\
</div>\
<div class="col-md-4">\
<button type="button" id="2" class="btn" onclick="" rel="tooltip" title="first tooltip">Поиск</button>\
</div>\
</div>';

app.contactListTemplate = '<div class="list-group" style="background-color: #c6e2ff;min-width:300px;max-width:300px;min-height:480px;max-height:480px;overflow: auto">\
</div>';

app.messagePanelTemplate = '<div class="row" style="background-color: #e0ffff;min-width:480px;max-width:480px;min-height:450px;max-height:450px;overflow: auto">\
</div>';

app.sendPanelTemplate = '<div class="row" style="background-color: #c6e2ff;min-width:480px;max-width:480px;min-height:100px;max-height:100px">\
    <div class="col-md-8" style="margin-left: 0px">\
        <textarea class="form-control" rows="3"></textarea>\
    </div>\
    <div class="col-md-2">\
        <button type="button" id="3" class="btn btn-default" onclick="">Отправить<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></button>\
    </div>\
</div>';

//<input type="text" class="form-control" placeholder="Введите текст">\

// app.messageTemplateRecipient = '<div class="row" style="margin-top: 5px; margin-left: 0px;max-width: 220px; min-width: 220px" >\
//     <blockquote class="pull-left">\
//         <div class="alert alert-success" role="alert"><%= item.message %></div>\
//         <small><%= item.time %></small>\
//     </blockquote>\
// </div>';
//
// //<small><%= item.time %> <%= item.recipient_name%></small>\
//
// app.messageTemplateSender = '<div class="row" style="margin-top: 5px; margin-left: 220px; max-width: 220px; min-width: 220px">\
//         <blockquote class="pull-right">\
//         <div class="alert alert-info" role="alert">\
//             <p class="text-left"><%= item.message %></p>\
//         </div>\
//        <small><%= item.time %></small>\
// </blockquote>\
// </div>';
app.messageTemplate = '<div class="row" style="margin-top: 5px; \
margin-left: <%=item.sender_id===app.me.id?220:0%>px; \
max-width: 220px; min-width: 220px">\
        <blockquote class="pull-<%=item.sender_id===app.me.id?"right":"left"%>">\
        <div class="alert alert-<%=item.sender_id===app.me.id?"info":"success"%>" role="alert">\
            <p class="text-left"><%= item.message %></p>\
        </div>\
       <small><%= item.time %></small>\
</blockquote>\
</div>';

//<small><%= item.time %> <%= item.recipient_name%></small>\

app.contactTemplate = '<a href="#" style="border: none" id=<%=item.recipient_id%> \
class="list-group-item<%=item.selected?" active":""%>">\
<%= item.nameRecipient %>\
<% if (item.messagesCount) {%>\
<span class="badge"><%=item.messagesCount%></span>\
<%}%>\
</a>';

// app.contactTemplateWithoutCount = '<a href="#" style="border:none" id=<%=item.recipient_id%> class="list-group-item<%=item.selected?" active":""%>"><%= item.nameRecipient %></a>';

//<a href="" style="background-color: #c6e2ff;border: none" id=<%=item.recipient_id%> class="list-group-item" ><%= item.nameRecipient %><span class="badge"><%= item.messagesCount %></span></a>\

//<a href="" style="background-color: #c6e2ff;border: none" id=<%=item.recipient_id%> class="list-group-item" ><%= item.nameRecipient %><span class="badge"><%= item.messagesCount %></span></a>\
//style="background-color: #c6e2ff;border: none" id=<%=item.recipient_id%>   <div class="list-group-item active" ><%= item.nameRecipient %><span class="badge"><%= item.messagesCount %></span></div>\
