/**
 * Created by andrei on 08.01.2016.
 */

app.BaseView = Backbone.View.extend({
    initialize    : function() {
        this.setElement(this.template());
    },
    empty   : function(){
        this.$el.empty();
    },
    append  : function(element){
        this.$el.append(element);
    },
    loadView: function(view){
        this.empty();
        this.append(view.el);
    },
    appendView: function(view){
        this.append(view.el);
    },
    render  : function() {
        return this;
    }
});

app.MainView = app.BaseView.extend({
    el      : $('#main'),
    initialize    : function() {},
    render          : function(){
        this.empty();

        this.contactPanel = new app.ContactPanelView().render();
        this.appendView(this.contactPanel);

        this.chatPanel = new app.ChatPanelView().render();
        this.appendView(this.chatPanel);
    }
});



app.ContactPanelView = app.BaseView.extend({
    template    : _.template(app.contactPanelTemplate),
    events: {
        "click .btn"                : "click_search_button",
        "click .list-group-item"    : "select_contact_from_contact_list"
    },
    select_contact_from_contact_list: function (event) {
        this.contactList.collection.findWhere({recipient_id:Number(app.selected_contact.id)}).set({'selected':false})
        this.contactList.collection.findWhere({recipient_id:Number(event.currentTarget.id)}).set({'selected':true})
        app.selected_contact.id = event.currentTarget.id;
        Backbone.trigger('select-contact-from-contact-list');
        ajaxCallSetSeen(this.contactList.collection);
    },
    click_search_button: function (event) {
        var filter_str = $("input:text").val();
        if(filter_str == ""){
            this.contactList.collection.url = app.Settings.getContactListUrl(app.me.id);
        }else{
            this.contactList.collection.url = app.Settings.getFilteredContactsUrl(filter_str, app.me.id);
        }
        this.contactList.draw();
    },
    render      : function(){
        this.contactSearch = new app.ContactSearchView().render();
        this.appendView(this.contactSearch);
        this.contactList = new app.ContactListView().render();
        this.appendView(this.contactList);
        return this;
    }
});

app.ChatPanelView = app.BaseView.extend({
    template    : _.template(app.chatPanelTemplate),
    render      : function(){

       // message
        this.messagePanel = new app.MessagePanelView().render();
        this.appendView(this.messagePanel);

        // send
        this.sendPanel = new app.SendPanelView().render();
        this.appendView(this.sendPanel);

        return this;
    }
});

app.ContactSearchView = app.BaseView.extend({
    template        : _.template(app.contactSearchTemplate),
    render          : function(){
        return this;
    }
});

app.ContactListView = app.BaseView.extend({
    template    : _.template(app.contactListTemplate),
    collection  : new app.CollectionContacts(),
    initialize: function() {

        this.setElement(this.template());
        this.listenTo(this.collection, 'reset', this.render);
        var that = this;
        app.timerNewMessages = setInterval(function() {
            ajaxCallNewMessage(that);
        }, 2000);

        this.draw();

    },
    //onChange:
    //    function(model, val, options)
    //    {
    //        var prev = model.previousAttributes();
    //        this.log( model.get("messagesCount") + " changed his name from " + prev.messagesCount);
    //    },
    draw    : function() {  // вытягивает данные с сервера
        this.collection.fetch({reset: true});
            //{remove: false});
    },
    add  : function(model){
        this.appendView(new app.ContactView({model: model}).render());
    },
    render      : function(){
        var that = this;
        this.empty();
        this.collection.each(function(model){
           if (model.get('id') != app.me.id) that.add(model);
         });
        return this;
    }
});

function ajaxCallNewMessage(that) {
    Backbone.ajax({
        dataType: "json",
        url: app.Settings.getNewMessagesArray(app.me.id),
        data: "",
        success: function (val) {

            var Model = app.NewMessages.extend({});

            var CollectionNM = app.CollectionNewMessages.extend({
                model: Model
            });
            setCountToContactModel(new CollectionNM(val),that);
        }
    });
}

function ajaxCallSetSeen(that) {
    Backbone.ajax({
        dataType: "json",
        url: app.Settings.updateNewMessagesCount(app.me.id,app.selected_contact.id),
        data: "",
        success: function (val) {

         var currentContact  = that.findWhere({recipient_id:Number(app.selected_contact.id)});
            if(currentContact != undefined){
                currentContact.set("messagesCount",0);
            }
        }
    });
}



function setCountToContactModel(arrayNM,that){
    arrayNM.each(
        function(modelNM){
            var xModel = that.collection.findWhere({recipient_id:modelNM.get("recipient_id")});
            if(xModel != undefined && (xModel.get("messagesCount") != modelNM.get("messageCount"))){
                xModel.set("messagesCount",modelNM.get("messageCount"));
            }
        }
    )
}

app.MessagePanelView = app.BaseView.extend({
    template    : _.template(app.messagePanelTemplate),
    collection      : new app.CollectionMessages(),
    initialize: function() {
        this.setElement(this.template());
        this.listenTo(this.collection, 'reset', this.render);

        Backbone.on('select-contact-from-contact-list', this.redraw, this);

        this.draw();
        app.timerId = setInterval(_.bind(this.redraw, this), 1000);
    },
    draw: function() {  // вытягивает данные с сервера
        this.collection.fetch({reset: true});
    },
    render      : function(){
        var that = this;
        this.empty();
        this.collection.each(function(model) {
            that.appendView(new app.MessageView({model: model}).render());
        });
        this.el.scrollTop = this.el.scrollHeight;
        return this;
    },
    redraw    : function(){
        this.collection.url = app.Settings.getMessagesUrl(app.me.id, app.selected_contact.id);
        this.draw();
    }
});

app.SendPanelView = app.BaseView.extend({
    template    : _.template(app.sendPanelTemplate),
    events: {
        "click .btn" : "submit1",
        "keydown textarea" : "submit2"

    },
    submit2: function(e) {
      if(e.ctrlKey && e.keyCode == 13) this.submit1();
    },
    submit1: function (event) {

        var SendMessageToServer = new app.MessageModel;
        if($("textarea").val()!=""){
            SendMessageToServer.set("sender_id", app.me.id);
            SendMessageToServer.set("recipient_id", app.selected_contact.id);
            SendMessageToServer.set("message", $("textarea").val());
            SendMessageToServer.set("time", new Date().getTime());
            SendMessageToServer.save();
            $("textarea").val("");
        }
    },
    render      : function(){
        return this;
    }
});

app.MessageView = app.BaseView.extend({
    template    : _.template(app.messageTemplate),
    initialize    : function() {
        this.setElement(this.template({item: this.model.toJSON()}));
    },
    render      : function(){
        return this;
    }
});

app.ContactView = app.BaseView.extend({
    template    : _.template(app.contactTemplate),
    initialize    : function() {
        this.setElement(this.template({item: this.model.toJSON()}));
        this.model.on('change', this.redraw, this);

    },
    redraw  : function(){
        var newElem = $(this.template({item: this.model.toJSON()}));
        this.$el.replaceWith(newElem);
        this.setElement(newElem);
        //this.loadView(this.render());
    },
    render      : function(){
        return this;
    }
});
